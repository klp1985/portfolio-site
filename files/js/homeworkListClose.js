//Close button fuctionality in homework view.  Removes class homework items and shows class list tiles.
$(document).ready(function() {
    $("#close-homework-list").click(function() {
        $(".class-list").css("display","inherit");
        $("#homework-view").css("display","none");
        $("#close-homework-list").css("display","none");
    });
});