//Control for the "School" tab dropdown menu.
$(document).ready(function(){
    $('.show-school').click(function(){
      $('#school-menu').stop(true).slideToggle('fast');
    });
//Close the dropdown by clicking anywhere outside the menu
    $(document).click(function (e) {
      if (!$(e.target).closest('.show-school, #school-menu').length) {
          $('#school-menu').stop(true).slideUp();
      }
  });
});


