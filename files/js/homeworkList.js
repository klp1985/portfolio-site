//Builds a class homework view in the "homework-view" div
$(document).ready(function() {
    var classObject;
    var classHeading;
    var homworkHeading;
    var projectsHeading;
    var homeworkObject;
    var projectsObject;
    
    //When a course is selected in the navigation bar or from a course tile a list of homework and project items will be displayed.
    //Page will auto-scroll to the homework section 
    $(this).on('click', '.course-item, .mainNavListItem', function() {
        $('html,body').animate({
            scrollTop: $("#scroll-school").offset().top
        }, 2000);

        //Class information and links to homework assignments are stored in .json files.  There is one for each class.
        //Get the location of a class .json file
        var putURL = $(this).closest('div.getID, li.getID').data("class-path");

        $.ajax({
            type: 'GET',
            url: putURL,
            error: function() {
               alert("Error");
            },
            dataType: 'text',
            //Gets all class information from JSON file and places it onto the page.
            success: function(data) {  
                classObject = $.parseJSON(data);  //Convert to JSON object
                classHeading = classObject.Title;
                homeworkHeading = classObject.Homework_Heading;
                projectsHeading = classObject.Projects_Heading;
                homeworkObject = classObject.Homework;
                projectsObject = classObject.Projects;
                 $("#homework-view").css("display","inherit");
                 $("#close-homework-list").css("display","block");
                 $("#class-header").html("");
                 $("#homework-list").html("");
                 $("#projects-list").html("");
                 $("#homework-heading").html("");
                 $("#projects-heading").html("");
                 $(".class-list").css("display","none");
                 $("#class-header").append("<h2>" + classHeading + "</h2><p>&nbsp;</p>");
                 $("#homework-heading").append("<h3>" + homeworkHeading + "</h3>");
                 $("#projects-heading").append("<h3>" + projectsHeading + "</h3>");
                 
                 //Builds list of homework items
                 $.each(homeworkObject, function(keyLvlOne,valueLvlOne){
                     $("#homework-list").append("<h4>" + keyLvlOne + "</h4>")
                     if ($.isArray(valueLvlOne)) {
                         $.each(valueLvlOne, function(keyLvlTwo, valueLvlTwo) {  
                              $("#homework-list").append("<p><a target='_blank' href='" + valueLvlTwo.link + "'>" + valueLvlTwo.name + "</a></p>");    
                         });
                     } else {
                         $("#homework-list").append("<p><a target='_blank' href='" + valueLvlOne.link + "'>" + valueLvlOne.name + "</a></p>");
                     }
                     $("#homework-list").append("<p>&nbsp;</p>");
                 });
                 
                 //Builds a list of projects items
                 $.each(projectsObject, function(keyLvlOne,valueLvlOne){
                     $("#projects-list").append("<h4>" + keyLvlOne + "</h4>")
                     if ($.isArray(valueLvlOne)) {
                         $.each(valueLvlOne, function(keyLvlTwo, valueLvlTwo) {  
                              $("#projects-list").append("<p><a target='_blank' href='" + valueLvlTwo.link + "'>" + valueLvlTwo.name + "</a></p>");    
                         });
                     } else {
                         $("#projects-list").append("<p><a target='_blank' href='" + valueLvlOne.link + "'>" + valueLvlOne.name + "</a></p>");
                     }
                     $("#projects-list").append("<p>&nbsp;</p>");
                 });
            }
        });
    });   
});