$(document).ready(function(){
    var scrollTop = 0;
    $(window).scroll(function(){
      scrollTop = $(window).scrollTop();
       //$('.counter').html(scrollTop);
      
      if (scrollTop >= 220) {
        $('#nav-menu').addClass('navbar-scroll');
        $('#leftside-nav-tri').addClass('triangle-scroll');
        $('#logo-container').addClass('logo-container-scroll');
      } else if (scrollTop < 220) {
        $('#nav-menu').removeClass('navbar-scroll');
        $('#leftside-nav-tri').removeClass('triangle-scroll');
        $('#logo-container').removeClass('logo-container-scroll');
      }   
    });   
  });