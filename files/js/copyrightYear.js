/*Copyright Date Javascript Function*/
/*Keeps the year in the footer current*/

$(document).ready(function(){		//Places the current year within the footer to make sure the copyright year is current.
	var date = new Date();
	var getYear = date.getFullYear();
	$("#copyDate").html(getYear);
});