$(document).ready(function() {
    var classNames = [];
    var classNumbers = [];
    var classPath = [];   
    var buildBtn = "";

    //The list of all courses is stored in the "class_info.json" file
    $.getJSON("files/json/class_info.json", function(data) {
        $.each(data, function(key,object) {    

            var storePath;
            classNames[key] = object.class_name;
            classNumbers[key] = object.class_number;
            classPath[key] = object.info_path;   
           
            buildBtn += `
            <div id="${classNumbers[key].replace(/ /g,'')}" class="getID" data-class-path="${classPath[key]}">
                <div class = 'course-item'>
                    <h3>${classNumbers[key]}: ${classNames[key]}</h3>
                </div>
            </div>
            `
            
            //Place the list of courses into the navigation menu dropdown
            $("#school-menu").append("<li class='mainNavListItem getID' data-class-path='" + classPath[key] + "'><a href='#classPages'>" + classNumbers[key] + ":" + classNames[key] + "</a><li>");
        });
        //Place the course tiles onto the page
        $(".class-list").append(buildBtn);
    });
});