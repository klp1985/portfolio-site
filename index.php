<?php
	session_start();

	$validForm = false;

	//PHPMailer version 6.0.3
	use PHPMailer\PHPMailer\PHPMailer;
	use PHPMailer\PHPMailer\Exception;

	function sendEmail($inEmail, $inName, $inComments, $contactDate, $resultMsg, $validForm) {	
		//Load composer's autoloader
		require 'vendor/autoload.php';
		$config = parse_ini_file('../config.ini');
		$mail = new PHPMailer(true);                          // Passing `true` enables exceptions
		//Server settings
		$mail->SMTPDebug = 2;                                 // Enable verbose debug output
		$mail->isMail();                                      // Set mailer to use SMTP
		$mail->Host = 'mail.heartland-webhosting.com';        // Specify main and backup SMTP servers
		$mail->SMTPAuth = true;                               // Enable SMTP authentication
		$mail->Username = $config['username'];                // SMTP username
		$mail->Password = $config['password'];                // SMTP password
		$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
		$mail->Port = 465;                                    // TCP port to connect to

		//Recipients
		$mail->setFrom('webdev@kraigpopelka.info', 'Kraig Popelka');
		$mail->addAddress($inEmail, $inName);     // Add a recipient
		$mail->addReplyTo('webdev@kraigpopelka.info', 'Kraig Popelka');
		$mail->addBCC('kpopelka@gmail.com');

		//Content
		$mail->isHTML(true);                                  // Set email format to HTML
		$mail->Subject = 'Your recent contact to Kraig Popelka - Web Developer';
		$mail->Body    = 'Dear ' .$inName. ',<br>Your message: "' .$inComments. '" was received at ' .$contactDate. '.  Expect a prompt response at your provided email address ('.$inEmail.').<br>Thank you,<br>Kraig Popelka';
		$mail->AltBody = "Thank you for your message.  It has been successfully received, and you can expect a prompt response.";

		if(!$mail->send()) {
			$resultMsg = "<div class = 'form-container'><h2 style = 'color: red; text-align: center; margin 15px;'>ERROR: There was a problem sending your message.  Please try again.</p></div>";
			$_SESSION['resultMsg'] = $resultMsg;
		} else {
			$resultMsg = "<div class = 'form-container msg-confirm'>";
			$resultMsg .= "<h2>Woo! Your message was successfully sent!</h2>";
			$resultMsg .= "<hr />";
			$resultMsg .= "<h1>Thank You, " . $inName . "!</h1>";
			$resultMsg .= "<p>Your message of '" . $inComments . "' was received at " . $contactDate . ".";
			$resultMsg .= "<p>You should receive an email confirmation shortly at " . $inEmail . " and you can expect a prompt response.</p>";
			$resultMsg .= "<hr />";
			$resultMsg .= "</div>";
			$_SESSION['resultMsg'] = $resultMsg;
		}
	}

	function getDateTime() {
		global $contactDate;
		date_default_timezone_set('America/Chicago');
		$contactDate = date('h:i A \o\n m/d/y');
	}

	function validateName() {
		global $inName, $validForm, $nameErrMsg;
		$nameErrMsg = "";
		if ( !$inName == "") {
            if(1 === preg_match('~[0-9]~', $inName)){
                $validForm = false;
				$nameErrMsg = "Your name can not have a number.";
				$_SESSION['name'] = $inName;
				$_SESSION['nameErrMsg'] = $nameErrMsg;
            } else {
				$inName = ltrim($inName);
				$inName = stripslashes($inName);
				$inName = htmlspecialchars($inName);
				$inName = filter_var($inName, FILTER_SANITIZE_STRING);
            }
		} else {
			$validForm = false;
			$nameErrMsg = "Your name is required.";
			$_SESSION['name'] = $inName;
			$_SESSION['nameErrMsg'] = $nameErrMsg;
		}
	}

	function validateEmail() {
		global $inEmail, $validForm, $emailErrMsg;
		$emailErrMsg = "";
		if ( !$inEmail == "") {
			$inEmail = filter_var($inEmail, FILTER_SANITIZE_EMAIL);
			if (filter_var($inEmail, FILTER_VALIDATE_EMAIL) === false) {
				$validForm = false;
				$emailErrMsg = "Email address is not formatted correctly.  Use the format jdoe@example.com";
			}
		} else {
				$validForm = false;
				$emailErrMsg = "Email address is required.";
		}
	}

	function validateComments() {
		global $inComments, $validForm, $commentsErrMsg;
		$commentsErrMsg = "";
		if ($inComments == "") {
			$validForm = false;
			$commentsErrMsg = "A comment is required for the reason selected.";
		} else {
			$inComments = trim($inComments);
  			$inComments = stripslashes($inComments);
  			$inComments = htmlspecialchars($inComments);
		}
	}

	function validatePhauxn() {
		global $inPhone, $validForm;
		if (!$inPhone == "") {
			$validForm = false;
		}
	}

	if ( isset($_POST['submitBtn']) ) {				//Checks if the form has been submitted.  If it has it will validate the form.
		$inName = $_POST['fullName'];
		$inEmail = $_POST['email'];
		$inComments = $_POST['comments'];
		$inPhone = $_POST['phone'];

		$validForm = true;

		validateName();
		validateEmail();
		validateComments();
		validatePhauxn();
		getDateTime();

		if ($validForm) {
			sendEmail($inEmail, $inName, $inComments, $contactDate, $resultMsg, $validForm);
		}
	}
?>

<!DOCTYPE html>
	<html lang="en">
		<head>
			<meta charset = "utf-8"/>
			<!--Kraig Popelka Web Development Porfolio
				Author:  Kraig Popelka
				Date Created:  1/15/2016 
				Date Modified: 3/6/2018
			-->
			<title>Kraig Popelka: Web Dev Porfolio</title>
			<meta name = "description" content = "Kraig Popelka is currently a web development intern at Iowa State University.  He recently received an associate degree in web development from Des Moines Area Community College and is seeking full-time employment." />
			<meta name = "keywords" content = "Kraig Popelka, DMACC, portfolio, school, work, web developer, html, css, javascript, sql, drupal, wordpress, php, java, ajax" />
			<meta name="viewport" content="initial-scale=1.0, width=device-width" />
			<link rel = "icon" type = "image/png" href = "images/theme/logo.png"/>
			<link rel = "shortcut icon" type = "image/png" href = "images/theme/logo.png"/>
			<link rel = "apple-touch-icon" type = "image/png" href = "images/theme/logo.png"/>
			<script type='text/javascript'src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE=" crossorigin="anonymous"></script>
				
			<script type='text/javascript' src="files/js/copyrightYear.js"></script>
			<script type='text/javascript' src="files/js/navscroll.js"></script>
			<script type='text/javascript' src="files/js/menuToggle.js"></script>
			<script type='text/javascript' src="files/js/classList.js"></script>
			<script type='text/javascript' src="files/js/homeworkList.js"></script>
			<script type='text/javascript' src="files/js/homeworkListClose.js"></script>

			<script type='text/javascript' src="vendor/fontawesome-all.min.js"></script>
			<script>
  				FontAwesomeConfig = { autoAddCss: false }
			</script>
			
			<script> //Google Analytics
				(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
				(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
				m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
				})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
				ga('create', 'UA-105633638-1', 'auto');
				ga('send', 'pageview');
			</script>
			<link href = "files/css/grid.css" rel = "stylesheet" type = "text/css" />
			<link href = "files/css/page_style.css" rel = "stylesheet" type = "text/css" />
			<link href = "files/css/navbar_style.css" rel = "stylesheet" type = "text/css" />
			<link href = "files/css/content_style.css" rel = "stylesheet" type = "text/css" />
			<link href = "files/css/projects_style.css" rel = "stylesheet" type = "text/css" />
			<link href = "files/css/courses_style.css" rel = "stylesheet" type = "text/css" />

			<script type='text/javascript'> 
					$(document).ready(function(){
						$('#about-link').on('click', function() {
							$('html,body').animate({
								scrollTop: $("#scroll-about").offset().top
							}, 1000);
						});
						$('#works-link').on('click', function() {
							$('html,body').animate({
								scrollTop: $("#scroll-works").offset().top
							}, 2000);
						});
						$('#contact-link').on('click', function() {
							$('html,body').animate({
								scrollTop: $("#scroll-contact").offset().top
							}, 2500);
						});
					});	
			</script>
		</head>
		<body>		
		  	<div id="particles-js"></div>
		  	<script type='text/javascript' src="https://cdn.jsdelivr.net/npm/particles.js@2.0.0/particles.min.js"></script>
		  	<script type='text/javascript' src="files/assets/app.js"></script>		
<!-- Navigation .................................................... -->
			<div class="page-top">
				<div id="leftside-nav-tri" class="triangle"></div>
				<nav id="nav-menu" class= "navbar">	
					<label for="show-menu" class="show-menu" onclick="toggleHam(this)">
						<div class="ham-icon">
							<div class="bar1"></div>
							<div class="bar2"></div>
							<div class="bar3"></div> 
						</div> <!-- End ham-icon -->
							<script>
								function toggleHam(x) {
									x.classList.toggle("change");
								}
							</script>
					</label>
					<input type="checkbox" id="show-menu" role="button">
					<div id="logo-container"><a href="index.php"><img src="images/theme/logo.png" title="Kraig Popelka KP Logo" alt="Logo consisting of letters K and P" /></a></div>			

					<ul id="menu">
						<li id="about-link" class = "top-link"><a class="navbar-link" href="#about-section">About</a></li>
						<li id="works-link" class = "top-link"><a class="navbar-link" href="#works">Works</a></li>
						<li id="school-link" class = "top-link show-school"><a class="navbar-link" onclick="$('#school-menu').stop(true).slideUp();return false" href="#">School &#10507;</a>
							<ul id = "school-menu" class="hidden"></ul>
						</li>
						<li id="contact-link" class = "top-link"><a class="navbar-link" href="#contact">Contact</a></li>
					</ul>
				</nav>
						
			</div> 
			<p>&nbsp;</p>
			<div class = "content">
				<h1 id="site-title">Kraig Popelka</h1>
				<h2 id="sub-title">Web Developer</h2>
				<div id="scroll-about"></div>
<!-- About section ....................................................-->
				<div id = "below-fold-content">
					<section id = "about-section">
						<h1 id="about-heading" class="section-banner">About</h1>
						<div id= "about-content" class = "content-region">	
							<p><img id="about-pic" src="images/theme/DU7A9882.jpg" alt="Kraig and his two dogs, Koopa and Rusty" title="Kraig Website Pic" /></p>
							<hr>
							<p>I am a web developer from Ames, Iowa and recently worked for the Biology IT group at Iowa State University where I develop university web pages in Drupal. Recently, I earned an Associates degree in web development from Des Moines Area Community College.  I have a background in science and have found out that web programming is like doing many small experiments.  You make a hypothesis that a line of code you added will result in what you thought the code would do.  Then you test it and find out it did something completely different.  Then you go back and repeat the process knowing that each time something doesn't work out you learn something new (or find that mispelled variable or misplaced semi-colon).</p>
							<p>&nbsp;</p>
							<p>Through my experience so far, I have found out that I enjoy both the coding and design aspects fairly equally.  I find working through a coding problem enjoyable as well as a great challenge.  Even though I have completed my formal training in web development and received my associates degree, I know that my education is just beginning.  Since there is a wide range goals for a website and how to accomplish them, I am constantly learning new techniques and technologies to help reach those goals.</p>
						</div> <!-- End content-region -->

						<div id= "skills-content" class = "content-region">
							<h1>Web Development Skills</h1>
							<h3>Proficient In:</h3>
							<ul class="removeBullets">
								<li class="skill-icon">
									<img src="images/theme/icons/html.svg" title="HTML5 Logo" alt="HTML5 Logo" />
									<p>HTML5</p>
								</li>
								<li class="skill-icon">
									<img src="images/theme/icons/css.svg" title="CSS3 Logo" alt="CSS3 Logo" />
									<p>CSS3</p>
								</li>
								<li class="skill-icon">
									<img src="images/theme/icons/javascript.svg" title="Javascript Logo" alt="Javascript Logo" />
									<p>Javascript</p>
								</li>
								<li class="skill-icon">
									<img src="images/theme/icons/jquery.svg" title="jQuery Logo" alt="jQuery Logo" />
									<p>jQuery</p>
								</li>
								<li class="skill-icon">
									<img src="images/theme/icons/ajax.svg" title="AJAX Logo" alt="AJAX Logo" />
									<p>AJAX</p>
								</li>
								<li class="skill-icon">
									<img src="images/theme/icons/php.svg" title="PHP Logo" alt="PHP Logo" />
									<p>PHP</p>
								</li>
								<li class="skill-icon">
									<img src="images/theme/icons/mysql.svg" title="MySQL Logo" alt="MySQL Logo" />
									<p>MySQL</p>
								</li>
								<li class="skill-icon">
									<img src="images/theme/icons/drupal.svg" title="Drupal Logo" alt="Drupal Logo" />
									<p>Drupal</p>
								</li>
								<li class="skill-icon">
									<img src="images/theme/icons/wordpress.svg" title="Wordpress Logo" alt="Wordpress Logo" />
									<p>Wordpress</p>
								</li>
								<li class="skill-icon">
									<img src="images/theme/icons/sass.svg" title="Sass Logo" alt="Sass Logo" />
									<p>Sass</p>
								</li>
								<li class="skill-icon">
									<img src="images/theme/icons/bootstrap.svg" title="Bootstrap Logo" alt="Bootstrap Logo" />
									<p>Bootstrap</p>
								</li>
								<li class="skill-icon">
									<img src="images/theme/icons/photoshop.svg" title="Photoshop Logo" alt="Photoshop Logo" />
									<p>Photoshop</p>
								</li>
							</ul>
							<div id="dabbled-skills">
								<h3>Experience with:</h3>
								<ul class="removeBullets">
									<li>Git</li>
									<li>React</li>
									<li>Angular</li>
									<li>Parcel</li>
									<li>Webpack</li>
									<li>Selenium</li>
									<li>Paypal integration</li>
									<li>Shopify</li>
									<li>Java</li>
									<li>Python</li>								
									<li>Cookies</li>
									<li>Data-Driven Documents (D3.js)</li>
									<li>Foundation</li>
									<li>Google Analytics</li>
									<li>Digital Marketing</li>
									<li>Regular Expressions</li>										
								</ul>
							</div>
						</div><!-- End skills-content content-region -->
					</section> <!-- End about -->			
<!-- Works section...................................................-->
					<div id="scroll-works" class="scroll-position"></div>
					<section id = "works">
						<h1 id="works-heading" class="section-banner">Works</h1>
							<div class = "project-list grid-wrapper">		
								<div id="ciam-card" class = 'project-item'>
									<a href="section/work/ia_aero_mod/drupal/" target="_blank">
										<img class = 'hideImg' src = 'images/projects/drupalCiam.png' />
										<h3>Central Iowa Aeromodelers</h3>
										<h4>Drupal</h4>
									</a>
								</div> <!-- End project-item -->

								<div id="phone-card" class = 'project-item'>
									<a href="section/classes/wdv341/projects/final/storeHome.php" target="_blank">
										<img class = 'hideImg' src = 'images/projects/phpEcomm.png' />
										<h3>E-Commerce Site</h3>
										<h4>PHP / MySQL / Bootstrap</h4>
									</a>
								</div> <!-- End project-item -->

								<div id="recipe-card" class = 'project-item'>
									<a href="section/classes/wdv321/projects/dynamicRecipe/recipeProject.html" target="_blank">
										<img class = 'hideImg' src = 'images/projects/recipeApp.png' />
										<h3>Recipe Application</h3>
										<h4>Javascript / jQuery</h4>
									</a>
								</div> <!-- End project-item -->
								
								<div id="brew-card" class = 'project-item'>
									<a href="section/classes/wdv321/projects/final/index.html" target="_blank">
										<img class = 'hideImg' src = 'images/projects/advjsFinal.png' />
										<h3>Deja Brew</h3>
										<h4>Javascript / PHP / Ajax</h4>
									</a>
								</div> <!-- End project-item -->

								<div id="cauli-card" class = 'project-item'>
								<a href="courses/wdv240/final/" target="_blank">
										<img class = 'hideImg' src = 'images/projects/wordpressFinal.png' />
										<h3>Cauli-Wood Art Gallery</h3>
										<h4>Wordpress</h4>
									</a>
								</div> <!-- End project-item -->

								<div id="thai-card" class = 'project-item'>
									<a href="section/classes/wdv221/projects/final/adTiming.html" target="_blank">
										<img class = 'hideImg' src = 'images/projects/jsFinal.png' />
										<h3>Downtown Thai</h3>
										<h4>Javascript</h4>
									</a>
								</div> <!-- End project-item -->
							
								<div id="edu-card" class = 'project-item'>
									<a href="section/classes/wdv495/projects/dataVis/eduViz.html" target="_blank">
										<img class = 'hideImg' src = 'images/projects/dataVizProj.png' />
										<h3>Education Data Visualization</h3>
										<h4>Javascript / Data Driven Documents (D3)</h4>
									</a>
								</div> <!-- End project-item -->

								<div id="photo-card" class = 'project-item'>
									<a href="section/classes/wdv205/projects/final/finalProject.html" target="_blank">
										<img class = 'hideImg' src = 'images/projects/cssFinal.png' />
										<h3>Photography Portfolio</h3>
										<h4>CSS / Sass</h4>
									</a>
								</div> <!-- End project-item -->

								<div id="halloween-card" class = 'project-item'>
									<a href="section/classes/wdv321/projects/teamProject/AJAXTeamProjectForm.html" target="_blank">
										<img class = 'hideImg' src = 'images/projects/jqueryHalloween.png' />
										<h3>Halloween Application</h3>
										<h4>Javascript / jQuery / Ajax</h4>
									</a>
								</div> <!-- End project-item -->
							
								<div id="business-card" class = 'project-item'>
									<a href="section/classes/wdv205/projects/foundationApp/foundationApp.html" target="_blank">
										<img class = 'hideImg' src = 'images/projects/cssFoundationProj.png' />
										<h3>Generic Business Site</h3>
										<h4>CSS / Foundation</h4>
									</a>
								</div> <!-- End project-item -->

								<div id="ad-card" class = 'project-item'>
									<a href="section/classes/wdv205/projects/advertEffectProj.html" target="_blank">
										<img class = 'hideImg' src = 'images/projects/cssAdEffect.png' />
										<h3>Advertising Effect</h3>
										<h4>CSS / Sass</h4>
									</a>
								</div> <!-- End project-item -->
							</div> <!-- End project-list -->
					</section> <!-- End works -->
<!-- Class Homework Pages........................................................-->
					<div id="scroll-school" class="scroll-position"></div>
					<section id = "classPages">
						<h1 id="school-heading" class="section-banner">DMACC Course Pages</h1>
							<div id="school-view">
								<div class = "class-list"></div>	
								<div id="homework-view">
									<div class="grid-homework-heading">										
										<div id="class-header" class="class-header"></div>
										<div id="class-list-close-btn" class="close-btn"><h4 id="close-homework-list"><i class="far fa-window-close"></i></h4></div>
									</div> <!-- End grid-homework-heading -->
									<div class="homework-grid-wrapper">
										<div id="homework">
											<div id ="homework-heading"></div>
											<div id ="homework-list"></div>
										</div> <!-- End homework -->
										<div id="projects">
											<div id ="projects-heading"></div>
											<div id ="projects-list"></div>
										</div> <!-- End projects -->
									</div> <!-- End homework-grid-wrapper -->
								</div><!-- End homwwork-view -->
								<div id="other-classes">
									<h3>Other Courses Completed:</h3>
									<ul class="removeBullets">
											<li>Drupal</li>
											<li>Wordpress</li>
											<li>Java</li>
											<li>Database and SQL</li>
											<li>Photoshop for Web</li>
											<li>Digital Marketing</li>
											<li>Principles of Digital Photography</li>
											<li>Photoshop for Photography</li>
									</ul>
								</div> <!-- End other-classes -->
							</div> <!-- End school-view -->
					</section> <!-- End class_pages -->
<!--Contact ...............................................................-->
				<section id = "contact">
					<div id="scroll-contact" class="scroll-position"></div>
					
						<h1 id="contact-heading" class="section-banner">Contact</h1>
							<div id="contact-region" class = "contact-region">
<?php
		if ($validForm) {	//If the form has been entered and validated a confirmation page is displayed in the VIEW area.
			echo $_SESSION['resultMsg'];
			session_unset();	//remove all session variables related to current session
			session_destroy();	//remove current session
		} else {  // Display the form if the user is seeing it for the first time or has submitted the form and failed validation.
?>
								<div class="form-container">	
									<p>Please fill out the form below to send me a message.</p>
										<form name="form1" method="post" action="index.php#contact">
											<p>
												<label>Name:  <span class = "error"><?php echo $nameErrMsg; ?></span><br>
													<input type="text" name="fullName" id="fullName" value="<?php echo $inName;?>" >												
												</label>
											</p>
											<p>Email:   <span class = "error"><?php echo "$emailErrMsg"; ?></span><br>
												<input type="text" name="email" id="email" value="<?php echo $inEmail;?>" >
											</p>
											<p>
												<label>Message: <br>   <span class = "error"><?php echo "$commentsErrMsg"; ?></span><br>
													<textarea name="comments" id="comments" cols="45" rows="5" ><?php echo $inComments;?></textarea>
												</label>
											</p>
											<p class = "containField">
												<label>Phone:  <br>
													<input type="text" name="phone" id="phone">
												</label>
											</p>
											<p>
												<input class = "form-buttons" type="submit" name="submitBtn" id="submitBtn" value="Send">
											</p>
										</form>
								</div> <!-- End form-container -->
<?php
		} // End else loop
?>
								
								<div class="contact-links">
									<h1><i class="fab fa-bitbucket"></i>    Bitbucket Repository</h1>
									<a class = "contact-buttons" target='_blank' rel="noopener noreferrer" href = "https://bitbucket.org/klp1985/">View Repository</a>
									<h1>&nbsp;</h1>
									<h1><i class="fas fa-file-alt"></i>    Resume</h1>
									<a class = "contact-buttons" target='_blank' href = "files/documents/Popelka_resume.pdf">View Resume</a><p>&nbsp;</p>
									<h1>&nbsp;</h1>
									<h1><i class="fab fa-linkedin"></i>    LinkedIn Profile</h1>
									<div id="linkedin-icon" class="contact-buttons">
										<script src="//platform.linkedin.com/in.js" type="text/javascript"></script>
										<script type="IN/MemberProfile" data-id="https://www.linkedin.com/in/kraig-popelka-791738141" data-format="hover" data-related="false"></script>
									</div>
								</div> <!-- End contact-links -->
							</div> <!-- End contact-region -->
						<div id="footer">
							<p>Copyright &copy; <span id = "copyDate"></span> Kraig Popelka. All Rights Reserved</p>
						</div>
					</section> <!-- End contact -->	
				</div> <!-- End below-fold-content -->
			</div> <!-- End content -->
		</body>
	</html>